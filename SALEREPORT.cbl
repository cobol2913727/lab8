       IDENTIFICATION DIVISION.
       PROGRAM-ID.  MYPROG.
       AUTHOR. Patchara L. 

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "FOODSALE.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
           SELECT 200-REPORT-FILE ASSIGN TO "SALE.RPT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-REPORT-FILE-STATUS.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS .
       01 100-INPUT-RECORD.
          05 BRANCH-ID              PIC X(1).
          05 FILLER                 PIC X(7).
          05 FOOD-ID                PIC X(4).
          05 FILLER                 PIC X(8).
          05 FOOD-NAME              PIC X(23).
          05 FOOD-PRICE             PIC 9(4).
          05 FILLER                 PIC X(5).
          05 FOOD-QUANTITY          PIC 9(1).
          05 FILLER                 PIC X(7).
          05 SALE-DATE.
             10 SALE-YEAR           PIC 9(4).
             10 FILLER              PIC X(1).
             10 SALE-MONTH          PIC 9(2).
             10 FILLER              PIC X(1).
             10 SALE-DAY            PIC 9(2).
          05 FILLER                 PIC X(1).
          05 SALE-TIME              PIC X(5).
          05 FILLER                 PIC X(4). 

       FD  200-REPORT-FILE
           BLOCK CONTAINS 0 RECORDS .
       01 200-REPORT-BUFFER         PIC X(80).    

       WORKING-STORAGE SECTION. 
       01 WS-INPUT-FILE-STATUS      PIC X(2).
          88 FILE-OK                          VALUE '00'.
          88 FILE-AT-END                      VALUE '10'.
       01 WS-REPORT-FILE-STATUS     PIC X(2).
          88 FILE-OK                          VALUE '00'.
          88 FILE-AT-END                      VALUE '10'.
      *COPY 'WS00293.CPY' REPLACING WS00293 BY WS-INPUT-FILE-STATUS.
       01 WS-CALCULATION.
          05 WS-INPUT-COUNT         PIC S9(5).
          05 WS-SALE.
             10 WS-BRANCH-ID        PIC X(1).
             10 WS-FOOD-ID          PIC X(4).
             10 WS-FOOD-NAME        PIC X(23).
             10 WS-FOOD-PRICE       PIC 9(4).
             10 WS-FOOD-QUANTITY    PIC 9(1).
             10 WS-SALE-DATE.
                15 WS-SALE-YEAR     PIC 9(4).
                15 FILLER           PIC X(1)  VALUE '-'.
                15 WS-SALE-MONTH    PIC 9(2).
                15 FILLER           PIC X(1)  VALUE '-'.
                15 WS-SALE-DAY      PIC 9(2).
             10 WS-SALE-TIME        PIC X(5).
          05 WS-SALE-SUMMARY.
             10 WS-TOTAL            PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-A          PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-B          PIC 9(8)  VALUE ZERO.
             10 WS-TOTAL-C          PIC 9(8)  VALUE ZERO.
          05 RPT-SALE-HEADER            PIC X(80) VALUE "DATE        TIM
      -    "E CODE            NAME              PRICE    UNIT".
          05 RPT-SALE-LINE.
             10 RPT-SALE-DATE.
                15 RPT-SALE-YEAR    PIC 9(4).
                15 FILLER           PIC X(1)  VALUE '-'.
                15 RPT-SALE-MONTH   PIC 9(2).
                15 FILLER           PIC X(1)  VALUE '-'.
                15 RPT-SALE-DAY     PIC 9(2).
             10 FILLER              PIC X(1)  VALUE SPACE.
             10 RPT-SALE-TIME       PIC X(5).
             10 FILLER              PIC X(1)  VALUE SPACE.
             10 RPT-FOOD-ID         PIC X(4).
             10 FILLER              PIC X(8)  VALUE SPACE.
             10 RPT-FOOD-NAME       PIC X(23).
             10 RPT-FOOD-PRICE      PIC 9(4).
             10 FILLER              PIC X(5)  VALUE SPACE.
             10 RPT-FOOD-QUANTITY   PIC 9(1).
          05 RPT-SALE-TOTAL-HEADER  PIC X(80) VALUE
                "BRANCH        TOTAL".
          05 RPT-SALE-TOTAL-LINE.
             10 RPT-BRANCH-ID       PIC X(1).
             10 FILLER              PIC X(12) VALUE SPACE.
             10 RPT-TOTAL           PIC 9(8)  VALUE ZERO.

       PROCEDURE DIVISION .
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT 
           PERFORM 2000-PROCESS THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END THRU 3000-EXIT 
           GOBACK 
           .
       1000-INITIAL.
           PERFORM 1100-OPEN-FILE-REPORT THRU 1100-EXIT 
           PERFORM 1200-OPEN-FILE-INPUT THRU 1200-EXIT 
           MOVE RPT-SALE-HEADER TO 200-REPORT-BUFFER 
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT 
           .
       1000-EXIT.
           EXIT
           .

       1100-OPEN-FILE-REPORT.
           OPEN OUTPUT 200-REPORT-FILE
           IF FILE-OK OF WS-REPORT-FILE-STATUS
              CONTINUE
           ELSE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              DISPLAY "* PARA 1100-OPEN-FILE-REPORT FAIL *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS: " WS-REPORT-FILE-STATUS " *"
                 UPON CONSOLE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .
       1100-EXIT.
           EXIT
           .     

       1200-OPEN-FILE-INPUT.
           OPEN INPUT 100-INPUT-FILE 
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              DISPLAY "* PARA 1200-OPEN-FILE-INPUT FAIL *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS: " WS-INPUT-FILE-STATUS " *"
                 UPON CONSOLE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT 
           .
       1200-EXIT.
           EXIT
           .

       2000-PROCESS.
           PERFORM 2100-MOVE-SALE THRU 2100-EXIT
           PERFORM 2300-MOVE-TO-SALE-REPORT-LINE THRU 2300-EXIT
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT 
           DISPLAY WS-SALE 
           PERFORM 2200-SUM-TOTAL-SALE THRU 2200-EXIT
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT 
           .
       2000-EXIT.
           EXIT
           .
       
       2100-MOVE-SALE.
           MOVE BRANCH-ID OF 100-INPUT-RECORD
              TO WS-BRANCH-ID OF WS-SALE 
           MOVE FOOD-ID OF 100-INPUT-RECORD
              TO WS-FOOD-ID OF WS-SALE 
           MOVE FOOD-NAME OF 100-INPUT-RECORD
              TO WS-FOOD-NAME OF WS-SALE 
           MOVE FOOD-PRICE OF 100-INPUT-RECORD
              TO WS-FOOD-PRICE OF WS-SALE 
           MOVE FOOD-QUANTITY OF 100-INPUT-RECORD
              TO WS-FOOD-QUANTITY OF WS-SALE 
           MOVE SALE-DATE OF 100-INPUT-RECORD
              TO WS-SALE-DATE OF WS-SALE 
           MOVE SALE-DAY OF 100-INPUT-RECORD
              TO WS-SALE-DAY OF WS-SALE 
           MOVE SALE-MONTH OF 100-INPUT-RECORD
              TO WS-SALE-MONTH OF WS-SALE 
           MOVE SALE-YEAR OF 100-INPUT-RECORD
              TO WS-SALE-YEAR OF WS-SALE 
           MOVE SALE-TIME OF 100-INPUT-RECORD
              TO WS-SALE-TIME OF WS-SALE 
           .
       2100-EXIT.
           EXIT.
       2200-SUM-TOTAL-SALE.
           COMPUTE WS-TOTAL =
              WS-TOTAL +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           EVALUATE WS-BRANCH-ID 
           WHEN 'A'
                COMPUTE WS-TOTAL-A =
                   WS-TOTAL-A +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           WHEN 'B'
                COMPUTE WS-TOTAL-B =
                   WS-TOTAL-B +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           WHEN 'C'
                COMPUTE WS-TOTAL-C =
                   WS-TOTAL-C +(WS-FOOD-PRICE * WS-FOOD-QUANTITY)
           END-EVALUATE 
           .

       2200-EXIT.
           EXIT.

       2300-MOVE-TO-SALE-REPORT-LINE.
           MOVE WS-SALE-DATE OF WS-SALE
              TO RPT-SALE-DATE OF RPT-SALE-LINE
           MOVE WS-SALE-TIME OF WS-SALE
              TO RPT-SALE-TIME OF RPT-SALE-LINE
           MOVE WS-FOOD-ID OF WS-SALE
              TO RPT-FOOD-ID OF RPT-SALE-LINE
           MOVE WS-FOOD-NAME OF WS-SALE
              TO RPT-FOOD-NAME OF RPT-SALE-LINE
           MOVE WS-FOOD-PRICE OF WS-SALE
              TO RPT-FOOD-PRICE OF RPT-SALE-LINE
           MOVE WS-FOOD-QUANTITY OF WS-SALE
              TO RPT-FOOD-QUANTITY OF RPT-SALE-LINE
           MOVE RPT-SALE-LINE TO 200-REPORT-BUFFER 
           .

       2300-EXIT.
           EXIT.    
       3000-END.
           MOVE SPACE TO 200-REPORT-BUFFER 
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT 
           PERFORM 3100-MOVE-TOTAL-SALE THRU 3100-EXIT 
           DISPLAY 'READ ' WS-INPUT-COUNT ' RECORDS'
           DISPLAY 'TOTAL SALE: ' WS-TOTAL 
           DISPLAY 'TOTAL SALE BRANCH A: ' WS-TOTAL-A 
           DISPLAY 'TOTAL SALE BRANCH B: ' WS-TOTAL-B 
           DISPLAY 'TOTAL SALE BRANCH C: ' WS-TOTAL-C
           CLOSE 100-INPUT-FILE 200-REPORT-FILE 
           .
       3000-EXIT.
           EXIT
           .

       3100-MOVE-TOTAL-SALE.
           MOVE RPT-SALE-TOTAL-HEADER TO 200-REPORT-BUFFER 
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT 

           MOVE 'A' TO RPT-BRANCH-ID 
           MOVE WS-TOTAL-A TO RPT-TOTAL
           MOVE RPT-SALE-TOTAL-LINE TO 200-REPORT-BUFFER 

           MOVE 'B' TO RPT-BRANCH-ID 
           MOVE WS-TOTAL-B TO RPT-TOTAL
           MOVE RPT-SALE-TOTAL-LINE TO 200-REPORT-BUFFER 
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT 
           
           MOVE 'C' TO RPT-BRANCH-ID 
           MOVE WS-TOTAL-C TO RPT-TOTAL
           MOVE RPT-SALE-TOTAL-LINE TO 200-REPORT-BUFFER 
           PERFORM 7000-WRITE-REPORT THRU 7000-EXIT
           .
       3100-EXIT.
           EXIT
           .

       7000-WRITE-REPORT.
           WRITE 200-REPORT-BUFFER 
           IF FILE-OK OF WS-REPORT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              DISPLAY "* PARA 7000-WRITE-REPORT FAIL *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS: " WS-REPORT-FILE-STATUS " *"
                 UPON CONSOLE
              DISPLAY "***** SALEREPORE ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .

       7000-EXIT.
           EXIT.

       8000-READ-FILE-INPUT.
           READ 100-INPUT-FILE 
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD +1 TO WS-INPUT-COUNT 
           ELSE 
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "***** SALEREPORE ABEND *****"
                    UPON CONSOLE
                 DISPLAY "* PARA 8000-READ-FILE-INPUT FAIL *"
                    UPON CONSOLE
                 DISPLAY "* FILE STATUS: " WS-INPUT-FILE-STATUS " *"
                    UPON CONSOLE
                 DISPLAY "***** SALEREPORE ABEND *****"
                    UPON CONSOLE
                 STOP RUN
              END-IF 
           END-IF 
           .
       8000-EXIT.
           EXIT.
